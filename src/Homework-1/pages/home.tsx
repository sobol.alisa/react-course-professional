/* Core */
import React from 'react';

/* Components */
import { Nav } from '../components/Nav';

/* Instruments */
import { icons } from '../theme/icons/tag';

export const HomePage = () => {
    return (
        <section className="layout">
            <Nav />
            <section className="hero">
                <div className="title" >
                    <h1>Типсы и Триксы</h1>
                    <h2>React</h2>
                </div>
                <div className="tags">
                <span>
                    <icons.React />
                    React
                </span>
                </div>
            </section>
        </section>
    );
}