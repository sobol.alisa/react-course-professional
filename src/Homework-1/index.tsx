/* Core */
import React from 'react';
import { render } from 'react-dom';

/* Pages */
import { HomePage } from './pages/home';

/* instruments */
import './theme/main.scss'

render(<HomePage />, document.getElementById('root'));