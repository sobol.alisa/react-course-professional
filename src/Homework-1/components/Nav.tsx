/* Core */
import React from 'react';

/* Instruments */
import { icons } from '../theme/icons/nav';

export const Nav = () => {
    return (
        <nav className="nav">
            <strong> Т и Т </strong>
            <a> <icons.Home /> все темы </a>
            <a> <icons.Tag /> по тэгам </a>
            <a> <icons.Publish /> опубликовать </a>
            <a> <icons.Search /> поиск </a>
        </nav>
    );
};