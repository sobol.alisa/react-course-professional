// Core
import React from 'react';
import ReactDOM from 'react-dom';

const element = React.createElement('h1', null, 'Hi, I am a React element.');


console.log(element)

ReactDOM.render(element, document.getElementById('root'));
