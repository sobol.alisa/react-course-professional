// Core
import React from 'react';
import { render } from 'react-dom';

const node1 = 'Welcome in ';
const node2 = new Date().getFullYear(); // Текущий год
const node3 = '!';

render(
    <>
        <li> {node1}</li>
        <li> {node2}</li>
        <li> {node3}</li>
    </>
    , document.getElementById('root'));
